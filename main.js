/* 
 * Copyright (C) 2017 WJClub
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const TelegramBot = require('node-telegram-bot-api');
var parse = require('url').parse;
var request = require('request');

const token = process.argv[2];

var muted = {};

// create and start bot
global.bot = new TelegramBot(token, {polling: true});
console.log("[LOG]    Started the Telegram Bot.");

global.bot.onText(/\/mute$/, function(msg, match) {
    if (msg.chat.type === 'supergroup' || msg.chat.type === 'group') {
        global.bot.getChatAdministrators(msg.chat.id).then(function(res) {
            res.forEach(function(user) {
                if (user.user.id === msg.from.id) {
                    // is admin
                    
                    if ( muted[''+msg.chat.id] !== true) {
                        muted[''+msg.chat.id] = true;
                        console.log('Muted chat', msg.chat);
                        global.bot.sendMessage(msg.chat.id, 'Muted this chat.');
                    } else {
                        muted[''+msg.chat.id] = false;
                        console.log('Unmuted chat', msg.chat);
                        global.bot.sendMessage(msg.chat.id, 'Unmuted this chat.');
                    }
                }
            });
        });
    }
});

global.bot.on('message', function(msg){
    
    if ((msg.chat.type === 'supergroup' || msg.chat.type === 'group') && muted[''+msg.chat.id]  === true) {
        // delete Message
        
        request.post(parse('https://api.telegram.org/bot'+token+'/deleteMessage'), {
            form: {
                chat_id: msg.chat.id,
                message_id: msg.message_id
            }
        });
    }
});